package Operator;

import java.util.Scanner;

public class Calculate_Total_Salary {
	public static void main(String []args) {
		Scanner scan = new  Scanner(System.in);
		System.out.println(" Enter basic Salary :");
		double basic = scan.nextDouble();
		double hra = 0.25*basic;
		double ta = 0.2*basic;
		double da = 0.15*basic;
		double total = basic+hra+ta+da;
		System.out.println("Total salary :"+total);
	}

}
