package StaticVariable;

/*-static variable  automatically initizad  with default  values  based on datatype----*/
public class DataType {
	static int a;
	static double b;
	static char c;
	static boolean d;
	static String e;

	static void value() {
		System.out.println("DefualtValues:");
		System.out.println("int :" + DataType.a);
		System.out.println("double :" + DataType.b);
		System.out.println("char :" + DataType.c);
		System.out.println("boolean :" + DataType.d);
		System.out.println("Strinf :" + DataType.e);

	}

	public static void main(String[] args) {
		DataType.value();
	}

}
