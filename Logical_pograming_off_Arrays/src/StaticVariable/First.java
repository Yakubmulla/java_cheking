package StaticVariable;
/*-- It is recommended  to define get()and set() method to each variable  in the class--*/
public class First {
	static int a, b, c;

	static void setA(int a) {
		First.a = a;
	}

	static void setB(int b) {
		First.b = b;

	}

	static void setC(int c) {
		First.c = c;
	}

	static int getA() {
		return First.a;
	}

	static int getB() {
		return First.b;
	}

	static int getC() {
		return First.c;
	}

}

class Second {
	public static void main(String[] args) {
		First.setA(10);
		First.setB(20);
		First.setC(30);
		System.out.println("A Values :" + First.getA());
		System.out.println("B  Values :" + First.getB());
		System.out.println("C Values :" + First.getC());
	}
}
