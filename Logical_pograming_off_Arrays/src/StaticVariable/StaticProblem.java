package StaticVariable;

public class StaticProblem {
	static int a;
	static void setA(int a) {
		StaticProblem.a=a;
		
	}
	static int getA() {
		return StaticProblem.a;
	}
	public static void  main(String []args) {
		StaticProblem .setA(10);
		System.out.println("A Value:"+StaticProblem .getA());
	}

}
