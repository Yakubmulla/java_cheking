package StringExample;

//Use the correct method to print the length of the txt string.
public class B11 {
	public static void main(String[] args) {
		String txt = "Hello Yakub";
		System.out.println(txt.length());
		// Convert the value of txt to upper case.
		System.out.println(txt.toUpperCase());
		// Use the correct operator to concatenate two strings
		String firstName = " Yakub ";
		String lastName = " Mulla ";
		System.out.println(firstName + lastName);
		// Use the correct method to concatenate two strings:
		System.out.println(firstName.concat(lastName));
		// Return the index (position) of the first occurrence of "e" in the following
		// string:
		String code = " Hello Yakub Mulla ";
		System.out.println(code.indexOf("Y"));

	}

}
