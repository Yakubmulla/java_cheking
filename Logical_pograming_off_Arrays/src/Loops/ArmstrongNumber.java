package Loops;

import java.util.Scanner;

/*------------------------Check the given  number is ArmStrong Number ----------*/
public class ArmstrongNumber {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the Number ");
		int n = scan.nextInt();
		int temp, sum = 0, r;
		temp = n;
		while (n > 0) {
			r = n % 10;
			sum = sum + r * r * r;
			n = n / 10;
		}
		if (temp == sum)
			System.out.println("ArmStrong Number");
		else
			System.out.println("Not an ArmStrong Number");
	}

}
