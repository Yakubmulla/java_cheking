package Loops;
import java.applet.*;
/*---Display sum of the Digit in the given number ----*/
import java.util.Scanner;
public class SumoftheDigit {
	public static void main(String []args) {
		Scanner scan =new Scanner(System.in);
		System.out.println("Enter Num : ");
		int n= scan.nextInt();
		int sum =0;
		while(n!=0) {
			sum =sum+n%10;
			n=n/10;
		}
		System.out.println("Sum of digit :"+ sum);
	}

}
