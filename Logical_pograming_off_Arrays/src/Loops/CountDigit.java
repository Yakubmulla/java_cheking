package Loops;

import java.util.*;

/* --- Count Digital Number for the Given number-------*/
public class CountDigit {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the Number :");
		int n = scan.nextInt();
		int count = 0;
		while (n != 0) {
			n = n / 20;
			count++;

		}
		System.out.println("Digit cout is :" + count);

	}

}
