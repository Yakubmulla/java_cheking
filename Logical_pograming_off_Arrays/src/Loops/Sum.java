package Loops;

import java.util.Scanner;
/*-----Multiple Number using that number----*/
public class Sum {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter  the Number :");
		int n = scan.nextInt();
		int multiple = 0;
		while (n != 0) {
			multiple = multiple + n % 10;
			n = n * 10;

		}
		System.out.println("Multiple  of the  Digit :" + multiple);
	}

}
