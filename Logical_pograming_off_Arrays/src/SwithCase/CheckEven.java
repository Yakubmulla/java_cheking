package SwithCase;

import java.util.Scanner;

/*------- Even Numbers util user exites --------*/
public class CheckEven {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		char res = 'n';
		do {
			System.out.println("Enter num");
			int n = scan.nextInt();
			if (n % 2 == 0)
				System.out.println(n + "iseven");
			else
				System.out.println(n + "is not even");
			System.out.println("Do you want cheak another num (y/n):");
			res = scan.next().charAt(0);
		} while (res == 'y');
	}
}
