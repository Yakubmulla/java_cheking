package ClassMember;

public class StaticPro {
	static {
		System.out.println("Static Block ");
	}

	public static void main(String[] args) {
		System.out.println("main Method");
	}

}
