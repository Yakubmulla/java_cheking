package ClassMember;

import java.util.*;

/*---------Class Member Like Static varibale and Instance varibale  method parameter -----------------*/
public class Employee {
	static String company = "TCS";// static variables
	static String adress = "hydarabad";
	int empid;// instance variables
	String empname;
	double empSalary;
	

	void totalSalary(double basic) {
		// method parametr : takes method input
		double hra = 0.2 * basic;// Local variable
		double ta = 0.15 * basic;
		double da = 0.25 * basic;
		double total = basic + hra + ta + da;
		System.out.println("total Salaray :" + total);
	}

	
}
