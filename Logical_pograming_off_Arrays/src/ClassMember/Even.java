package ClassMember;

/*---- with a same argument and no retrun values ----*/
public class Even {
	public static void main(String[] args) {
		Even.code(4);
		Even.code(13);
	}

	static void code(int n) {
		if (n % 2 == 0)
			System.out.println(n + " is Even number");
		else
			System.out.println(n + "is Not Even number");
	}

}
