package InstanceVarible;

/*---With arguments and  with  return  values  method ---*/
public class WithReturnValue {
	public static void main(String[] args) {
		WithReturnValue obj = new WithReturnValue();
		int r1 = obj.add(10, 20);
		System.out.println(r1);
	}

	int add(int a, int b) {
		return a + b;
	}

}
