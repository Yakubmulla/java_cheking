package InstanceVarible;

/*--No argument  and With  return Values----*/
public class NoArguments {
	public static void main(String[] args) {
		NoArguments obj = new NoArguments();
		double PI = obj.getPI();
		System.out.println("pi value :" + PI);
	}

	double getPI() {
		double PI = 3.142;
		return PI;
	}

}
