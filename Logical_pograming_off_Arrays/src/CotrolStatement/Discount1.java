package CotrolStatement;

import java.util.*;
/*-- in this program  we Decare the first1.----*/
public class Discount1 {
	public static void main(String[] args) {
		// give the 20% Discount if the bill amount is > 200000
		double bill = 30000;
		if (bill > 20000) {
			double discount = 0.2 * bill;
			bill = bill - discount;
			System.out.println("Total bill of customer is :" + bill);

		}
	}

}
