package CotrolStatement;

import java.util.*;

/*---- find to check  the character is Alphabet or Digit or Sysmbol---------*/
public class AlphabetOrDigitOrSymbol {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the Charactr :");
		char ch = scan.next().charAt(0);
		if ((ch >= 'A' && ch <= 'z') || (ch >= 'a' && ch <= 'z'))
			System.out.println("Alphabet");
		else if (ch >= '0' && ch <= '9')
			System.out.println("Digit");
		else
			System.out.println("Special Symbol");

	}

}
