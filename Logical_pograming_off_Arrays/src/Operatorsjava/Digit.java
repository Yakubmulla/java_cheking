package Operatorsjava;

/*------------------Find the last Digit  of Given  Number -------------------------------------*/
import java.util.*;

public class Digit {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the Number");
		int n = scan.nextInt();
		int d = n % 10;
		System.out.println(" Last Digit   of " + n + " is " + d);

	}
}
